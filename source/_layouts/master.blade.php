<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Jigsaw – Static Sites for Laravel Developers</title>

        <link rel="stylesheet" href="{{ $base }}/css/main.css">
    </head>
    <body>
          <div class="flex-center position-ref full-height">
              @yield('content')
          </div>
    </body>
</html>
